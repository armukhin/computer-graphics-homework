from moviepy.editor import *
import os

clip1 = VideoFileClip(os.path.dirname(os.path.abspath(__file__)) + "\\input1.mkv")
clip2 = VideoFileClip(os.path.dirname(os.path.abspath(__file__)) + "\\input2.mkv")

clip1.write_videofile(os.path.dirname(os.path.abspath(__file__)) + "\\input1.mp4")
clip2.write_videofile(os.path.dirname(os.path.abspath(__file__)) + "\\input2.mp4")

audioclip2 = AudioFileClip(os.path.dirname(os.path.abspath(__file__)) + "\\input2.mp4")
audioclip2.set_fps(96000)
clip1 = clip1.set_audio(audioclip2)
clip2 = clip2.set_audio(audioclip2)
clip1 = clip1.resize(16/9)
clip2 = clip2.resize(16/9)
clip1.write_videofile(os.path.dirname(os.path.abspath(__file__)) + "\\output1.mp4", bitrate = "12582912", fps = 48, codec = "libx265")
clip2.write_videofile(os.path.dirname(os.path.abspath(__file__)) + "\\output2.mp4", bitrate = "12582912", fps = 48, codec = "libx264")

file = open(os.path.dirname(os.path.abspath(__file__)) + "\\Difference_between_size_of_videos.txt", "w") 
print()
file.write("Difference between mkv files in bytes: " + str(os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\input1.mkv") - os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\input2.mkv")) + "\n")
file.write("Difference between mp4 files in bytes: " + str(os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\input1.mp4") - os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\input2.mp4")) + "\n")
file.write("Difference between modified files in bytes: " + str(os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\output1.mp4") - os.path.getsize(os.path.dirname(os.path.abspath(__file__)) + "\\output2.mp4")) + "\n")
file.close()